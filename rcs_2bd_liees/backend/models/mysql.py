from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Clients(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100))
    prenom = db.Column(db.String(100))
    email = db.Column(db.String(100))
    # Autres champs...

class Commandes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_client = db.Column(db.Integer, db.ForeignKey('clients.id'))
    date_commande = db.Column(db.Date)
    total = db.Column(db.Float)
    # Autres champs...

class Produits(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom_produit = db.Column(db.String(100))
    prix = db.Column(db.Float)
    categorie = db.Column(db.String(100))
    # Autres champs...

class CommandeProduits(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_commande = db.Column(db.Integer, db.ForeignKey('commandes.id'))
    id_produit = db.Column(db.Integer, db.ForeignKey('produits.id'))
    quantite = db.Column(db.Integer)
    # Autres champs...