

from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from pymongo import MongoClient

import logging

app = Flask(__name__)

# Configuration SQLAlchemy pour MySQL
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:@localhost/mydb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

# Configuration pour MongoDB
mongo_client = MongoClient("mongodb+srv://dabakhsalledu:Amadou@cluster0.ycia1gn.mongodb.net/?retryWrites=true&w=majority/")
mongo_db = mongo_client["mongodb_database"]

# Configuration du logger
logging.basicConfig(level=logging.DEBUG)  # Niveau DEBUG pour afficher tous les messages de logs

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    Prenom = db.Column(db.String(100))
    Age = db.Column(db.Integer)
    Sexe = db.Column(db.String(10))
    # Autres champs pour MySQL

# Route pour ajouter un utilisateur aux deux bases de données
@app.route('/add_user', methods=['POST'])
def add_user():
    data = request.get_json()
    new_user = User(
    name=data['name'],
    Prenom=data.get('Prenom'),
    Age=data.get('Age'),
    Sexe=data.get('Sexe')
)  

    try:
    # Tentative d'ajout dans la base de données MySQL
        db.session.add(new_user)
        db.session.commit()
        user_id = new_user.id  # Obtention de l'ID après l'insertion dans MySQL
        logging.debug(f"Utilisateur ajouté dans MySQL avec l'ID : {user_id}")

    # Tentative d'ajout dans la base de données MongoDB en utilisant l'ID de MySQL
        mongo_user = {
            "_id": user_id,
            "name": data['name']
            # Ajoutez d'autres champs MongoDB si nécessaire
        }
        result_mongo = mongo_db.users.insert_one(mongo_user)
        if result_mongo.inserted_id:
            logging.debug("Utilisateur ajouté dans MongoDB")

        return jsonify({'message': 'Utilisateur ajouté avec succès dans les deux bases de données.'}), 200

    except Exception as e:
    
        logging.exception(f"Erreur lors de l'ajout dans les bases de données : {str(e)}")
        return jsonify({'message': 'Erreur lors de l\'ajout dans les bases de données. Sauvegardé dans MongoDB.'}), 500


if __name__ == '__main__':
    app.run(debug=True)


# from flask import Flask
# from flask_sqlalchemy import SQLAlchemy

# app = Flask(__name__)

# # Configuration SQLAlchemy pour MySQL
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:@localhost/mydb'
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# db = SQLAlchemy(app)

# # Définition du modèle pour votre table
# class user(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(50))
#     Prenom = db.Column(db.String(50))
#     Age = db.Column(db.Integer)
#     Sexe = db.Column(db.String(1))

# # Insérer des données dans votre table
# @app.route('/add_user',methods=['POST'])
# def insert_data():
#     # Création d'une instance de votre table avec des données
#     nouvel_utilisateur = user(nom='John', prenom='Doe', age=30, sexe='M')

#     try:
#         # Ajouter les données à la session et commettre la transaction
#         db.session.add(nouvel_utilisateur)
#         db.session.commit()
#         return 'Données insérées avec succès dans la table.'
#     except Exception as e:
#         # En cas d'erreur, annuler les changements et afficher l'erreur
#         db.session.rollback()
#         return f'Erreur lors de l\'insertion des données : {str(e)}'

# if __name__ == '__main__':
#     app.run(debug=True)
