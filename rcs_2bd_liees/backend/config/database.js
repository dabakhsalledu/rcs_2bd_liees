//Connection a notre base de donnees MongoDb
const mongoose = require('mongoose');
const connectDb = async () => {
    try {
        await mongoose.connect(process.env.MONGO_DB_URL);
        console.log(`Connected to mongoDb`)
    } catch (err) {
        console.log(`Mongo err : ${err}`);
    }
}
module.exports = connectDb;