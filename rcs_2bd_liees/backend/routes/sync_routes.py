from flask import Blueprint, jsonify

sync_routes = Blueprint("sync_routes", __name__)

@sync_routes.route("/synchroniser", methods=["POST"])
def synchroniser_donnees():
    # Logique de synchronisation MySQL vers MongoDB
    # ...
    return jsonify({"message": "Synchronisation réussie"})