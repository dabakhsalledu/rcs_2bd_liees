import pymongo

def insert_into_mongodb():
    # Connexion à MongoDB
    client = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = client["mydb"]  # Remplacez par le nom de votre base de données
    mycol = mydb["Clients"]  # Création d'une collection appelée "Clients"

    # Définition d'un document à insérer dans la collection
    client1 = {
      "nom": "Dupont",
      "prenom": "Jean",
      "email": "jean.dupont@example.com",
      # Autres champs...
    }

    # Insertion du document dans la collection
    mycol.insert_one(client1)

    # Vous pouvez ajouter d'autres opérations avec MongoDB ici

if __name__ == "__main__":
    insert_into_mongodb()  # Exécuter la fonction pour insérer des données au lancement du script